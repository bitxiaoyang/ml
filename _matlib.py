import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import AutoMinorLocator,MultipleLocator,FuncFormatter


# x = [1, 2, 4, 7]
# y = [5, 6, 8, 10]
# x1 = [10, 12, 14, 18]
# y1 = [11, 15, 16, 19]

# lst1=[[0,1,2],[3,4,5],[6,7,8]]
# x2=np.array(lst1)
# # 传入两个列表，x为横坐标，y为纵坐标
# plt.plot(x, y, 'ro-', x1, y1, 'bo--')
# plt.show()


# lst1 = [[0, 1, 2], [3, 4, 5], [6, 7, 8]]
# lst2 = [[2, 3, 2], [3, 4, 3], [4, 5, 4]]
# # 传入两个列表，x为横坐标，y为纵坐标
# plt.plot(lst1, lst2,  'bo--')
# plt.show()


# x = [1, 2, 4, 7]
# y = [5, 6, 8, 10]
# # 蓝色,线宽20,圆点,点尺寸50,点填充红色,点边缘宽度6,点边缘灰色
# plt.plot(x, y, color="blue", linewidth=10, marker="o", markersize=50,
#          markerfacecolor="red", markeredgewidth=6, markeredgecolor="grey")
# plt.show()



# x = [1, 2, 4, 7]
# y = [5, 6, 8, 10]
#
# plt.subplot(224)
# plt.xlabel('size')
# plt.ylabel('price')
# plt.title('house price')
# plt.plot(x, y, 'ro')
#
# plt.show()


fig = plt.figure()
ax = fig.add_subplot(111)
plt.show()
