import numpy as np
import torch
import torch.nn.functional as F


def iou_score(output, target):
    smooth = 1e-5  # 平滑值

    if torch.is_tensor(output):
        output = torch.sigmoid(output).data.cpu().numpy()
    if torch.is_tensor(target):
        target = target.data.cpu().numpy()
    output_ = output > 0.5   # 预测值大于0.5为True
    target_ = target > 0.5  # 目标值大于0.5为True
    intersection = (output_ & target_).sum()   # 交集
    union = (output_ | target_).sum()          # 并集

    return (intersection + smooth) / (union + smooth)  # iou


def dice_coef(output, target):
    smooth = 1e-5

    output = torch.sigmoid(output).view(-1).data.cpu().numpy()
    target = target.view(-1).data.cpu().numpy()
    intersection = (output * target).sum()

    return (2. * intersection + smooth) / \
        (output.sum() + target.sum() + smooth)
